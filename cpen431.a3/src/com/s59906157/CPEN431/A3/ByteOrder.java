package com.s59906157.CPEN431.A3;

public class ByteOrder {

	/** 
	 * Little-endian bytes to int
	 * 
	 * @requires x.length-offset>=4
	 * @effects returns the value of x[offset..offset+4] as an int, 
	 *   assuming x is interpreted as a signed little endian number (i.e., 
	 *   x[offset] is LSB) If you want to interpret it as an unsigned number,
	 *   call ubytes2long on the result.
	 */
	public static int leb2int(byte[] x, int offset) {
		//Must mask value after left-shifting, since case from byte
		//to int copies most significant bit to the left!
		int x0=x[offset] & 0xFF;
		int x1=(x[offset+1]<<8) & 0xFF;
		//        int x2=(x[offset+2]<<16) & 0x00FF0000;
		//        int x3=(x[offset+3]<<24);
		//        return x3|x2|x1|x0;
		return x1+x0;
	}
	
	/** 
     * Little-endian bytes to int.  Unlike leb2int(x, offset), this version can
     * read fewer than 4 bytes.  If n<4, the returned value is never negative.
     * 
     * @param x the source of the bytes
     * @param offset the index to start reading bytes
     * @param n the number of bytes to read, which must be between 1 and 4, 
     *  inclusive
     * @return the value of x[offset..offset+N] as an int, assuming x is 
     *  interpreted as an unsigned little-endian number (i.e., x[offset] is LSB). 
     * @exception IllegalArgumentException n is less than 1 or greater than 4
     * @exception IndexOutOfBoundsException offset<0 or offset+n>x.length
     */
    public static int leb2int(byte[] x, int offset, int n) 
            throws IndexOutOfBoundsException, IllegalArgumentException {
        if (n<1 || n>4)
            throw new IllegalArgumentException("No bytes specified");

        //Must mask value after left-shifting, since case from byte
        //to int copies most significant bit to the left!
        int x0=x[offset] & 0x000000FF;
        int x1=0;
        int x2=0;
        int x3=0;
        if (n>1) {
            x1=(x[offset+1]<<8) & 0x0000FF00;
            if (n>2) {
                x2=(x[offset+2]<<16) & 0x00FF0000;
                if (n>3)
                    x3=(x[offset+3]<<24);               
            }
        }
        return x3|x2|x1|x0;
    }


}



