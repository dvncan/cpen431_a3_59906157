package com.s59906157.CPEN431.A3;

public class CopyArray {

	public CopyArray(){
		
	}
	
	public static byte[] copy( int ofst, byte[] a1, byte[] error, byte[] a3, byte[] a4){
		
		byte[] dst = new byte[(a1.length + 1 + a3.length + a4.length)];
		
		System.arraycopy(a1, 0, dst, ofst, a1.length);
		System.arraycopy(error, 0, dst, a1.length, 1);
		System.arraycopy(a3, 0, dst, (a1.length + 1), a3.length);
		System.arraycopy(a4, 0, dst, (a1.length + 1 + a3.length), a4.length);
		
		return dst;
	}

	public static byte[] copy(int ofst, byte[] a1, byte[] a2) {
		byte[] dst = new byte[(a1.length+a2.length)];
		
		System.arraycopy(a1, 0, dst, ofst, a1.length);
		System.arraycopy(a2, 0, dst, a1.length, a2.length);
		
		return dst;
	}

	
	
}
