package com.s59906157.CPEN431.A3;

import java.nio.ByteBuffer;
import java.util.Arrays;

public class Key {
	
	public Key() {
		
	}

	public static ByteBuffer setKey(ByteBuffer bb) {
		
		byte[] keyB = new byte[32];
		bb.position(17);
		bb.get(keyB);
		System.out.println("Key: " + Arrays.toString(keyB));
		
		return ByteBuffer.wrap(keyB);
	}

}
