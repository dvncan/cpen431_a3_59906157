package com.s59906157.CPEN431.A3;

public class Error {

	int val;
	byte[] erVal;
	
	public Error(){
		erVal = new byte[1];
		val = 1;
	}
	
	public void setVal(int i){
		val = i;
		
		erVal[0] = (byte) (val & 0xFF);
		
	}
	public byte getVal(){
		return erVal[0];
	}
}
