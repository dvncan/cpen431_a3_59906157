package com.s59906157.CPEN431.A3;

import java.io.*;
import java.net.*;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class ServerThread extends Thread{

	public ServerThread() throws IOException, ExceptionSystemOverload{

		this("ServerThread: ");

	}

	public ServerThread(String name) throws IOException, ExceptionSystemOverload{
		super();

		byte[] buffer = new byte[10051];
		byte[] replyArray = new byte[10019];
		byte[] error = new byte[1];
		byte[] replyId = new byte[16];
		int cmd;
		int port = 1256;
		boolean shutDown = false;
		DatagramSocket serverSocket = new DatagramSocket(16157);
		DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
		MyMap map = new MyMap();


		while(!shutDown){
			serverSocket.receive(packet);
			
			//Checking the received data (Bytes) for command
			//			System.out.println(Arrays.toString(packet.getData()));

			//Use a byteBuffer as navigation.
			ByteBuffer bb = ByteBuffer.wrap(buffer);

			//copy the request ID
			replyId = Value.getIdVal(bb);

			//Get the first byte of pay load, and determine what the command is.
			Command com = new Command(bb.get(16));
			cmd = com.checkCommand();

			if(cmd == 6){
				error[0] = (byte)(0x05);
				replyArray = CopyArray.copy(0, replyId, error);
			}else{

				//set key to be sent to map.
				ByteBuffer keyBuffer = Key.setKey(bb); 
				//				System.out.println("Key: " + keyBuffer.toString());

				//create byte array 
				byte[] value;
				byte[] replyLength = new byte[2];
				byte[] replyValue;

				if(cmd == 1){
					//determine the length of the value
					int length = ByteOrder.leb2int(buffer, 49, 2);

					//check value length 
					if(length > 10000 || length <= 0){
						error[0] = (byte)(0x06);
						replyArray = CopyArray.copy(0, replyId, error);
					}
					else{

						System.out.println("Value Length: "+ length);

						try{
							//get value
							value = Value.getVal(bb, length);
							
							//check packet for inconsistant value.
							Value.checkPacket(packet, length);
							
							//returns error code.
							error[0] = map.put(keyBuffer, length, value);
						}catch(OutOfMemoryError e){
							error[0] = (byte)(0x02);
						}catch(BufferUnderflowException u){
							error[0] = (byte)(0x21);
						}catch(ExceptionSystemOverload s){
							error[0] = (byte)(0x02);
						} catch (ExceptionInvalidValueLength e) {
							// TODO Auto-generated catch block
							error[0] = (byte)(0x21);
						}
						System.out.println("error: " + Arrays.toString(error));
						//wrap error and send. 
						replyArray = CopyArray.copy(0, replyId, error);
					}
				}
				else if(cmd == 2){
					error[0] = map.check(keyBuffer);
					System.out.println("Map Size: " + map.size());
					if(error[0] == (byte)0x00){
						replyValue = map.get(keyBuffer);
						replyLength = Value.valLength2bytes(replyValue.length);
						System.out.println("Reply Id: " + Arrays.toString(replyId) + " Error Code: " + Arrays.toString(error) + " Value Length: " + Arrays.toString(replyLength) + " Reply Value: " + Arrays.toString(replyValue));
						replyArray = CopyArray.copy( 0, replyId, error, replyLength, replyValue);	
					}else{
						//send away.
						replyArray = CopyArray.copy(0, replyId, error);
					}
				}
				else if(cmd == 3){
					error[0] = map.check(keyBuffer);
					if(error[0] == (byte)0x00){
						map.remove(keyBuffer);
						System.out.println("Map size: " + map.size());
						replyArray = CopyArray.copy(0, replyId, error);
					}else{
						//send away.
						replyArray = CopyArray.copy(0, replyId, error);
					}
				}
				else if(cmd == 4){
					shutDown = true;

				}else if(cmd == 5){
					map.clear();
					error[0] = (byte)(0x00);
					replyArray = CopyArray.copy(0, replyId, error);
				}else{
					error[0] = (byte)(0x05);
					replyArray = CopyArray.copy(0, replyId, error);
				}
			}
			port = packet.getPort();
			//			System.out.println("" + port);
			InetAddress ip = packet.getAddress();

			DatagramPacket sendPacket = new DatagramPacket(replyArray, replyArray.length, ip, port);

			System.out.println("Sent.");
			cmd = 0;
			serverSocket.send(sendPacket);

		}
		System.out.println("Server Exiting... ");
		serverSocket.close();
	}

}
